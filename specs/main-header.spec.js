var expect 			= require('chai').expect;
var	headerPage 		= require('../pageobjects/main-header.page');
var headerExpect	= require('../pageobjects/main-header-expect.page')
var bodyParser		= require("body-parser");

describe('Header Button Tests', function () {
	
	/**
	 * Anasayfada olduğumuzu belli eden onedio tag
	 * elementi tanımlaması.
	 */
	let mainTag		= ".logo";
	/**
	 * Sistem yüklenmesi yavaş oluyorsa waitTime arttır.
	 */
	let waitTime	= 50000; 
	this.timeout(waitTime);
	it('Test Button Test',function(){
		this.timeout(waitTime);
		headerPage.open();
    	headerPage.btnTest.click();
		expect(headerExpect.exTest.getText()).to.contain('TEST');
	});
	
	it('Video Button Test',function(){
		this.timeout(waitTime);
		headerPage.open();
		headerPage.btnVideo.click();		
		expect(headerExpect.exVideo.getText()).to.contain('VİDEO');
	});
	
	it('Food Button Test',function(){
		headerPage.open();
		this.timeout(waitTime);
		headerPage.btnFood.click();
		expect(headerExpect.exFood.getText()).to.contain('YEMEK');
	});
	
	it('Cafe Button Test',function(){
		this.timeout(waitTime);
		headerPage.open();
		headerPage.btnCafe.click();
		expect(headerExpect.exCafe.getText()).to.contain('CAFE');
	});
	/**
	 * Gündem buttonunda new tab açılıyor webdriverio ile halledildi.
	 */
	it('Gündem Button Test',function(){
		let windowHandles="";
		this.timeout(waitTime);
		headerPage.open();
		headerPage.btnGundem.click();
		windowHandles=browser.windowHandles();
		app.use(bodyParser.urlencoded({extended: true}));
		console.log("-----------------------------------");
		//console.log("Yeni pencere"+windowHandles[0]);
		//İlk açılan new tab e yönlendirilir.
		headerPage.switchTab(windowHandles["value"][1]);
		expect(headerExpect.exGundem).to.have.text('GÜNDEM');
	});
    
});

describe('Header Emoji Tests',function(){

	it('Emoji Smile Test',function(){
		headerPage.open();
		headerPage.emSmile.click();
		//expect	headerPage.flash.getText()).to.contain('Onedio Hesabı ile Giriş Yap');
	})

	it('Emoji Heart Test',function(){
		headerPage.open();
		headerPage.emHeart.click();
	})

	it('Emoji Rocket Test',function(){
		headerPage.open();
		headerPage.emRocket.click();
	});

	it('Emoji Clap Test',function(){
		headerPage.open();
		headerPage.emClap.click();
	});

	it('Emoji Surprize Test',function(){
		headerPage.open();
		headerPage.emSurprize.click();
	})
});
