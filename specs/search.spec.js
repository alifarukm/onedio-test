var expect = require('chai').expect;
var FormPage = require('../pageobjects/search.page');
var popup = require('../specs/popup.spec');
describe('search', function () {
    it('C19 should display search input field',function(){
        FormPage.open();
        FormPage.searchButton.click();
        console.log(FormPage.searchButton);
        FormPage.inputField.setValue('Onedio');
        browser.keys(['Enter']);
        expect(FormPage.flash.getText()).to.contain('Onedio');
    });
});